{-# LANGUAGE OverloadedStrings #-}

module Main where

import Color
import Control.Lens
import Control.Monad (unless)
import qualified Data.Text as T
import qualified Data.Vector.Storable as V
import Foreign.C.Types
import SDL (($=))
import qualified SDL
import SDL.Vect hiding (Vector)

main :: IO ()
main = do
  SDL.initializeAll
  window <- SDL.createWindow windowTitle SDL.defaultWindow {SDL.windowResizable = True}
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer
  loop window renderer
  shutdown window

-- | The window title
windowTitle :: T.Text
windowTitle = "Geometry"

-- | A 2D position
type Position = Point V2 CInt

-- | A 2D size
type Size = V2 CInt

-- | Renders multiple points
renderPoints :: SDL.Renderer -> V.Vector Position -> Color -> IO ()
renderPoints renderer positions color = do
  SDL.rendererDrawColor renderer $= color
  SDL.drawPoints renderer positions

-- | Renders a line
renderLine :: SDL.Renderer -> Position -> Position -> Color -> IO ()
renderLine renderer start end color = do
  SDL.rendererDrawColor renderer $= color
  SDL.drawLine renderer start end

-- | Renders a rectangle outline
renderRect :: SDL.Renderer -> Position -> Size -> Color -> IO ()
renderRect renderer position size color = do
  let rectangle = Just $ SDL.Rectangle position size
  SDL.rendererDrawColor renderer $= color
  SDL.drawRect renderer rectangle

-- | Renders a filled rectangle
renderFillRect :: SDL.Renderer -> Position -> Size -> Color -> IO ()
renderFillRect renderer position size color = do
  let rectangle = Just $ SDL.Rectangle position size
  SDL.rendererDrawColor renderer $= color
  SDL.fillRect renderer rectangle

-- | Renders a set of primitives
render :: SDL.Window -> SDL.Renderer -> IO ()
render window renderer = do
  windowSize <- SDL.get $ SDL.windowSize window
  let windowWidth = windowSize ^. _x
  let windowHeight = windowSize ^. _y
  let windowCenter = fmap (`div` 2) windowSize

  -- Renders a red filled rectangle in the center of the screen
  let rectSize = fmap (`div` 6) windowSize
  let rectHalfSize = fmap (`div` 2) rectSize
  let rectPosition = P $ windowCenter - rectHalfSize
  renderFillRect renderer rectPosition rectSize red

  -- Renders a blue rectangle outline in the center of the screen
  renderRect renderer rectPosition rectSize blue

  -- Renders a vertical green line in the center of the screen
  let lineStart = P $ V2 (windowWidth `div` 2) 0
  let lineEnd = P $ V2 (windowWidth `div` 2) windowHeight
  renderLine renderer lineStart lineEnd green

  -- Renders an horizontal dashed black line in the center of the screen
  let pointSpacing = 5
  let pointYPos = windowHeight `div` 2
  let pointXPos = [0, pointSpacing .. windowWidth]
  let pointsPos = V.fromList $ map (P . flip V2 pointYPos) pointXPos
  renderPoints renderer pointsPos black

-- | The main loop
loop :: SDL.Window -> SDL.Renderer -> IO ()
loop window renderer = do
  events <- SDL.pollEvents
  let quit = SDL.QuitEvent `elem` map SDL.eventPayload events
  SDL.rendererDrawColor renderer SDL.$= white
  SDL.clear renderer
  render window renderer
  SDL.present renderer
  unless quit $ loop window renderer

-- | Frees any resources
shutdown :: SDL.Window -> IO ()
shutdown window = do
  SDL.destroyWindow window
  SDL.quit
