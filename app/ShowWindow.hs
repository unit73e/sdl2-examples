{-# LANGUAGE OverloadedStrings #-}

module Main where

import Color
import Control.Monad (unless)
import qualified Data.Text as T
import SDL (($=))
import qualified SDL

-- | Shows a window with blank surface.
main :: IO ()
main = do
  SDL.initializeAll
  window <- SDL.createWindow windowTitle SDL.defaultWindow
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer
  loop renderer
  shutdown window

-- | The window title
windowTitle :: T.Text
windowTitle = "Show Window"

-- | The main loop
loop :: SDL.Renderer -> IO ()
loop renderer = do
  events <- SDL.pollEvents
  let quit = SDL.QuitEvent `elem` map SDL.eventPayload events
  SDL.rendererDrawColor renderer SDL.$= white
  SDL.clear renderer
  SDL.present renderer
  unless quit $ loop renderer

-- | Frees any resources
shutdown :: SDL.Window -> IO ()
shutdown window = do
  SDL.destroyWindow window
  SDL.quit
