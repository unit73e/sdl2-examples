{-# LANGUAGE OverloadedStrings #-}

module Main where

import Color
import Control.Monad (unless)
import qualified Data.Text as T
import Paths_sdl2_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image

-- | Shows a window with blank surface.
main :: IO ()
main = do
  SDL.initializeAll
  window <- SDL.createWindow windowTitle SDL.defaultWindow
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer
  texture <- loadTexture renderer "image.png"
  loop renderer texture
  shutdown window texture

-- | The window title
windowTitle :: T.Text
windowTitle = "Show Image"

-- | Loads a texture
loadTexture :: SDL.Renderer -> FilePath -> IO SDL.Texture
loadTexture renderer path = do
  image <- getDataFileName path >>= SDL.Image.load
  texture <- SDL.createTextureFromSurface renderer image
  SDL.freeSurface image
  return texture

-- | The main loop
loop :: SDL.Renderer -> SDL.Texture -> IO ()
loop renderer texture = do
  events <- SDL.pollEvents
  let quit = SDL.QuitEvent `elem` map SDL.eventPayload events
  SDL.rendererDrawColor renderer SDL.$= white
  SDL.clear renderer
  SDL.copy renderer texture Nothing Nothing
  SDL.present renderer
  unless quit $ loop renderer texture

-- | Frees any resources
shutdown :: SDL.Window -> SDL.Texture -> IO ()
shutdown window texture = do
  SDL.destroyTexture texture
  SDL.destroyWindow window
  SDL.quit
